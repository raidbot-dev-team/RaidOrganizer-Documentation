// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Raid Organizer Documentation',
  tagline: 'Raid Organizer',
  url: 'https://raidorganizer.org',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: '/', // Serve the docs at the site's root
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/raidbot-dev-team/RaidOrganizer-Documentation/-/tree/develop/',
        },
        blog: {
          showReadingTime: true,
          blogTitle: 'Sneak Peaks',
          blogSidebarTitle: 'All posts',
          blogSidebarCount: 'ALL',
          routeBasePath: 'sneak-peak',
        },
        theme: {
          customCss: [
            require.resolve('./src/css/custom.css'),
            require.resolve('./src/css/discord-message-component.css'),],
        },
        gtag: {
          trackingID: 'G-Q6LT833RRJ',
          anonymizeIP: true,
        },
      }),
    ],
  ],

  plugins:
   [
    /** @type {import('@cmfcmf/docusaurus-search-local')} */
    [
      require.resolve("@cmfcmf/docusaurus-search-local"),
      {
        indexBlog: false,
        maxSearchResults: 16,
      },
    ],
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'blog-patch-notes',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: 'patch-notes',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: './patch-notes',
      },
    ],
   ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      docs: {
        sidebar: {
          autoCollapseCategories: false,
        }
      },
      navbar: {
        title: 'Raid Organizer',
        logo: {
          alt: 'Raid Organizer Docs',
          src: 'img/doc_logo.png',
        },
        items: [
          {
            to: '/',
            label: 'Documentation',
            position: 'left'
          },
          {
            to: 'https://raidorganizer.org',
            label: 'Home',
            position: 'left'
          },
          {
            href: 'https://gitlab.com/raidbot-dev-team/RaidOrganizer-Documentation',
            label: 'Gitlab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'RAID ORGANIZER',
          },
          {
            title: 'LEGAL',
            items: [
              {
                label: 'Terms & Conditions',
                to: 'https://raidorganizer.org/terms',
              },
              {
                label: 'Privacy Information',
                to: 'https://raidorganizer.org/privacy',
              },
            ],
          },
        ],
        copyright: `©2020-${new Date().getFullYear()} Copyright: Raid Organizer`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
