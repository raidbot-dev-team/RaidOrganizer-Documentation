# About the Documentation

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator and deployed with [Netlify](https://www.netlify.com/)

# Local Development

If you do not want to use the gitlab GUI to change the docs, follow the steps below.

## Clone the git repository

```bash
git clone git@gitlab.com:raidbot-dev-team/raidorganizer-doc.git
```

This command clones the doc repo.

## Install Node.js

Docusaurus needs [Node.js](https://nodejs.org/en/download/) v18.0 or above.

## Install packages

`npm install`

## Start the site

```bash
npx docusaurus start
```

This command starts the site at `http://localhost:3000`.
