# Advertisement on Raid Organizer

## My Policy

I hate ads, and chances are high that you don't like them either. However, ads help a bit to fund this hobby project and I am happy that
you are interested in my policy and how I approach it.

Here are my key points taken into consideration before advertising for a product:
* I either use their service myself or I am very much convinced about the product (NOTE: Currently only the first one applies).
* No cross side tracking cookies
* No banner that is blinking or annoying in any other way

## Current Ads

<details>
  <summary>Proton (Mail/VPN)</summary>
  <div>
    <div>Proton is a privacy first provider for email, vpn, calendar, password manager and cloud storage. I personally use their email and vpn service.<br/>
    <br/>
    Click here to find their <a href="https://go.getproton.me/SHg6" target="_blank">mail</a> and <a href="https://go.getproton.me/SHg8" target="_blank">vpn</a> offer.
    </div>
  </div>
</details>

<details>
  <summary>Hetzner (VPS/Storage)</summary>
  <div>
    <div>Hetzner is a German server hosting provider. They offer a wide range of very reliable services. I use their VPS for all my projects (including Raid Organizer) and their storage share solution.<br/>
    <br/>
    Using my <a href="https://go.getproton.me/SHg6" target="_blank">sign up link</a>, you get 20€ worth of credits on sign up. This will buy your their cheapest cloud server for 5 Months.
    </div>
  </div>
</details>

<details>
  <summary>UptimeRobot</summary>
  <div>
    <div>UptimeRobot is a service to monitor your services and notifies you if your service is down. I use it to monitor all my public projects, including <a href="https://uptime.raidorganizer.org/" target="_blank">Raid Organizer</a>.<br/>
    <br/>
    You can find their offer <a href="https://uptimerobot.com/?rid=82613b2c2b76a4" target="_blank">here</a>.
    </div>
  </div>
</details>

<details>
  <summary>Hover (Domain)</summary>
  <div>
    <div>Hover is a domain name registrar. All my domains (including <a href="https://raidorganizer.org" target="_blank">raidorganizer.org</a>) are registered there.<br/>
    <br/>
    Using <a href="https://hover.com/fxcG33B9" target="_blank">this</a> link, you get 2$ off of your first purchase.
    </div>
  </div>
</details>

<details>
  <summary>Humblebundle (Books and Games)</summary>
  <div>
    <div>Humblebundle is a charity website where you can buy game, book and software bundles for a very cheap price. They work together with publishers to 1) offer bundles cheaper and 2) support a charity with those earnings. I buy a lot of my IT books there.<br/>
    <br/>
    You can find their offer <a href="https://www.humblebundle.com/?partner=snawe" target="_blank">here</a>.
    </div>
  </div>
</details>