---
title: Create Events
description: Create an Event
---

:::info
 To create an event you need at least one channel that is [set up](/getting-started/setup-channel). You can get all your set up channels with `/channel list`.
:::

## Create an Event

Use the `/event create` command to create an event.

:::note
 The fields `title` and `event_start` are mandatory. All other fields are optional!
:::

<img src={require('@site/static/gif/event_create_dark.gif').default} />

Once you have created an event you can:
* Sign up to it with the emojis below the event.
* Manage the event via the `Event ID` at the bottom of the event. Use the `/event xxx` commands and for that.

## Event Parameter

* title: The title of the event *
* event_start: The date/time of the event *
* event_end: The event end time. This field will convert to the user local time.
* description: A longer description of what event is about
* template: The template from which the event is created. This overwrites the channel default template
* text: A text message that is printed before the event itself. This can be used to @ping specific roles, since Discord does not allow pings in "embeds".
* event_template: Creates an event using the parameters set in the [event template](/event-templates/)
* footer_text: Additional text that will get printed in the event footer **
* footer_icon: A small icon on the left of the footer message **
* image: The big image at the bottom of the event message **
* thumbnail: A smaller image in the top right of the event message **

\* Are mandatory  
\*\* Are [premium](https://raidorganizer.org/premium) options

## Relative Date/Time Input

It might not always be easy for your users to use the full date/time string on event creation (e.g. `2023-05-14 20:00`).  
If your server has users in different timezones it might be even harder, because `20:00` in their **locale** timezone isn't the same as `20:00`in the **server** timezone.

Here is where relative date/time input shines!

Users can use the `event create` command with a `event_start/end` of `1d 3h` and no matter in which timezone they life, it _is_ in 1 day and 3 hours.  
If you want to create an event that starts right away, use the keyword `now`.

:::info
 If you use `now` as `event_start`, an event with a start time of _right now_ is created. This has the effect that the event will close automatically, if auto close is on!  
 To disable that events get closed automatically, use `/event_close toggle`.
:::

:::info
 Only the above options use the users timezone! If one of the options below are used (explicitly `today`, `tomorrow` or a specific time like `20:00`) the bot **uses** the **server timzone**!

 **Why?**  
 Because the bot can now know what the users `today`, `tomorrow` or `20:00` is! However, it can use the current UTC time and add `1d 3h` to it because there it does not matter what
 exactly the users current time is.
:::

So what are valid options and how to they resolve?
* `now` => creates an event with the date of today and a start/end time of _now_
* `today` => creates an event with the date of today and **no** start/end time
* `tomorrow` => creates an event with the date of tomorrow and **no** start/end time
* `1d 3h 30m` => Resolves to `from now in 1 day 3 hours and 30 minutes`. Any combination of X`d`, Y`h`, Z`m` is possible.


**Combine!**
You can combine `now`, `today` and `tomorrow` with either a precise time (like `08:00pm`) or a relative time (`1h 10m`)
Examples:
* `today 20:30` will resolve in the date of today and the start/end time of 20:30
* `today 1d 30m` will resolve to the date of tomorrow (`1d`) and the time of you executing the command + 30 minutes. So this would actually be the same as `tomorrow 30m`.

:::note
 Again, if you use either `today`, `tomorrow` or a specific time like `20:00` in the `event_start/end`, the **servers timezone** is take to calculate the exact start/end time.  
 If you use only a combination of `d`, `h` and `m`, the servers timezone **does not matter**.
:::
