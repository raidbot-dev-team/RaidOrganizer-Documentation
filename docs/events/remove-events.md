---
title: Remove Events
description: Remove an Existing Event
---

By default events will be deleted 14 days after the end time is reached. If the event does not have an end time, midnight will be used as a reference. You can change that by using `/event_deletion_time set`.

If you want to remove an event earlier, you can use the `/event remove` command.

:::info
 We try to keep as little data as possible stored in our database which means that events that are deleted (either manually or automatically) are gone for good!

 If you want to keep events for a longer period, we have a few ways for you:
 * Use `/event export` to export an (still existing) event.
 * Use `/export_channel set` and `/toggle export_event_on_close`  to automatically export events to a given channel as soon as the event closes for sign up.
:::
