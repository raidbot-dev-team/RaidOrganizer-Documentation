---
title: Edit Events
description: Edit an Existing Event
---

To edit an existing event, use the `/event edit` command with the `event_id`, select the `field` you want to edit and pass the new `value`.

<img src={require('@site/static/gif/event_edit_dark.gif').default} />
