import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

# Events

Let's learn about events and how to create, remove and manage them!

<DocCardList items={useCurrentSidebarCategory().items}/>

There are many more commands to manage events! For a full list checkout the  `/help` command inside Discord or the [help](https://raidorganizer.org/help) webpage and select the `Event` category.
