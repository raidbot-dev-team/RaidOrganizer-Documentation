---
description: Use Event Templates to make event creation even faster
keywords: [event template]
---

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

# Event Templates

Event templates make it easier and faster to create events.

## What Are Event Templates?

Basically they are an entry that holds all values you need for an event. Let's say you have an event like this:
* `title`: Guild meeting
* `description`: Let's discuss how we proceed in the next month!
* `start_time`: 20:00
* `template`: 2

And you have guild meetings regularly, but not like in a fixed interval (because then you could use [repeating events](/repeating-events)). So instead of adding all those values over and over again, you can [make an "Event Template"](./setup-event-template) and then [create events](./create-event-template) from that template.

:::note
 There are a lot more of use cases for event templates.
 
 For example if your guild is running an LFG channel and your guild mates are creating dungeon runs for the same ~3 dungeons over and over. You could provide them a template with the right sign up limits, dungeon name or even a banner (image) for that dungeon.
 
 The guild mates then just need to [create an event](./create-event-template) with the right date/start time. Not worrying about the sign up template, description, title, ...
:::

<DocCardList items={useCurrentSidebarCategory().items}/>