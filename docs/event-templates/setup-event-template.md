---
title: Setup Event Templates
description: Setup an Event Template
---

Use `/event_template setup` to create an event template.

:::info
 `title` is the only mandatory field for an event template because the `event_start` (which every event needs) will be set at event creation.
:::

<img src={require('@site/static/gif/event_template_setup_dark.gif').default} />

To create an event from the just created template, use the [`/event_template create`](./create-event-template) command.



