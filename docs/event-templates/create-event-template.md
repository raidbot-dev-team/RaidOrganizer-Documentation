---
title: Create Event from Template
description: Create an Event from a Template
---

Use `/event_template create` to create an event from an event template.

The following fields are mandatory:
* `template_id`: The event template to use for the event creation
* `event_start`: The date/time of the event

If you send the command with just those two parameters, an event with the settings of the template will be created.

<img src={require('@site/static/gif/event_template_create_dark.gif').default} />

As you can see, we now have an event with a start time of `20:00` and a description of `Let's discuss xxx` but we have only passed a `date`!

_But what if you need to adjust the event just a little bit?_

## Modify the Event 

Let's continue with [guild meeting](/event-templates/#what-are-event-templates) example from the "Event Template" introduction. We have an event template with the following parameters:
* `title`: Guild Meeting
* `description`: Let's discuss xxx
* `start_time`: 20:00
* `template`: 2

but our next guild meeting needs to start at _21:00_. Just create the event as usual, but also add a time to the `event_start` to override the default value.

:::note
 This time we create the event via the `event create` command. This is just to showcase that `event_template create` and `event create` with the `event_template` parameter are doing the same. Sadly (due to some Discord limitations), you need to pass a `title` if you are using the `event create` command.
:::

<img src={require('@site/static/gif/event_template_create_modified_dark.gif').default} />
