---
title: Remove Event Templates
description: Remove an Event Template
---

Use `/event_template remove` to remove an event template.

:::note
 Removing an event template does not remove events that are created from that template.
:::