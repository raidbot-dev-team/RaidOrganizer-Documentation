---
description: Sign Up Templates allow you to use custom emojis, limits and more!
keywords: [custom, template, sign up]
---

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

# Sign Up Templates

With sign up templates you can create your own custom templates using:
* Emojis of your choice
* Field names you want
* Tweak the sign up limits emoji and user based

Basically create an event that fits your need!

<DocCardList items={useCurrentSidebarCategory().items}/>