---
title: Edit Sign Up Templates
description: Edit a Custom Sign Up Template
---

Use the `/signup_template edit` command to edit an existing template.

:::warning
### Edit `emojis` or `emoji behavior`

If you edit either `emojis` or `emoji behavior` **all** events that are currently using the template will **stop working**.

**Why?**  
Because both options change crucial stuff regarding the event sign up.  
For example:
* If you remove an emoji or rename a field, users that sign up after the template change would be shown differently than users that signed up before the edit.
* If you change the `emoji behavior` most likely the (first level) sign up emojis would be different. If you change to `1`, all sub roles would also be directly displayed and if you change from `1` to `2` or `3`, emojis would be hidden.
:::
:::note
 If you edit `emojis` or `limits`, **no** `value` parameter is need!  
 Instead the bot will send you a DM and guide you through the changes.
:::