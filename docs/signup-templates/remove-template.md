---
title: Remove Sign Up Templates
description: Remove a Custom Sign Up Template
---

Use the `/signup_template remove` command to remove an existing template.

:::warning
 Once a template is removed, **all events** that have used the template will **stop working**.
:::