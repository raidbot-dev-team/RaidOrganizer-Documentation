---
title: Import Sign Up Templates
description: Import a Custom Sign Up template
---

Use the `/signup_template import` command to import a template.

Another way of using a custom template is to let someone else do the work ;)  
There is a dedicated channel in the official `Raid Organizer` [Discord server](https://discord.gg/cVzMUKX) called [🔄template-share](https://discord.gg/cVzMUKX) where users can share their custom templates.
:::note
 Please only post templates in there to keep this channel clean! Do not post requests.
:::
:::warning
 Sharing templates is great! But keep in mind: The bot can only use emojis from a server which it is a member of! This means:
 * If the bot gets kicked from the server which created the template
 * **Or** the emojis get removed from that server


 the template will no longer work as expected.
:::