---
description: User name type displayed in events
keywords: [nick, user, account, name]
---

# Display Name

A user in Discord can have several different types of names. The `/display_names set` command configures which
name is displayed in the event message.

## Server Nickname

This name is a server specific name. A user can have different names in each server.  
To change this name, go to:
* User Settings
* Profiles
* Server Profiles
* Server Nickname

<img src={require('@site/static/img/server_nickname.PNG').default} />

## User Display Name

This name is the default name of the user in all Discord server.  
To change this name, go to:
* User Settings
* Profiles
* User Profiles
* Display Name

<img src={require('@site/static/img/user_displayname.PNG').default} />

## Account Username

This is the Discord account name of the user. It is unique throughout Discord.  
To change this name, go to:
* User Settings
* My Account
* Username

<img src={require('@site/static/img/account_name.PNG').default} />

## @Mention

Simply the same as using @user_name in Discord. This has the same resolve order as the bot.  
Server Nickname > User Display Name > Account Username.

:::info
 Discord sometimes has some client side issues not resolving the names correctly. It sometimes happens that
 `@user` is displayed as `<@123456789>`. If that happens, just restart your client.
:::