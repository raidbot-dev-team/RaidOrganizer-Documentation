---
description: Configs that apply to all events inside a channel
keywords: [channel, config]
---

import DocCardList from '@theme/DocCardList';

# Channel Configurations

:::note
  If you haven't set up an event channel, check out the [getting started](/getting-started/setup-channel) guide.
:::

Channel configs are settings that apply to everything in the given channel. Below you can find some additional
documentation about some channel related commands.

<DocCardList />

:::info
For a full list checkout the `/help` command inside Discord or the [help](https://raidorganizer.org/help) webpage and select the `Channel` category.
:::