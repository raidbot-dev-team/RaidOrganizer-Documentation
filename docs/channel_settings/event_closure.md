---
title: Event Closure
description: Why do events close and what does it mean?
keywords: [event, close, automatic, open, closed]
---

## Why are my events closed?

By default `Raid Organizer` automatically closes events when the event `start time` is reached. If you want, you can disable this behavior via the `/event_close toggle` command.

:::info
  This is a channel setting. This means that it applies to all events in that channel. Like other channel settings, you can find the current state via `/channel config`.
::: 

## What does "closed" mean?

If an event is closed, users can no longer sign up/off from the event. This prevents people from sneaking into the event after it started or gives you a deadline where all users must be signed up.

## Can I change the time at which the event closes?

Yes! There are 2 things you can control:
* `/event_close behavior`: This changes if the event is closed relative to the `event start` or `event end`
* `/event_close time set`: This sets how long before the event start/end the event will be closed.

## Can I open already closed events?

Yes! There are 2 ways to open and event.

* Right click event message => Apps => Open Event.  
* `/event open`.

(Same applies for closing an event.)

:::warning
 If you reopen an event closed by the bot without disabling automatic closure, it will close again after a while.
:::
