---
title: Create Repeating Events
description: Create a Repeating Event
---

import {
  DiscordInteraction,
  DiscordMessage,
  DiscordEmbed,
  DiscordButtons,
  DiscordButton,
} from "@discord-message-components/react";
import "@discord-message-components/react/dist/style.css";

import DiscordComponent from "../../src/components/DiscordComponent";


## Create a Repeating Event 

To create a repeating event use the `/repeating_event create` command.

The following fields are mandatory:
* `title`: The title of the actual event that will be created.
* `event_start`: The date/time of the first event.
* `days`: Event `event_start` minus `days` is the date at which the event will be created.
* `creation_time`: The time at which the event will be created.

:::note
If you create a repeating event with `event_start` _2022-05-30_, `days` _7_ and `creation_time` _12:00_, the event will be created on _2022-05-23_ at _12:00_ with the event date set to _2022-05-30_.
:::

In the example below we are creating an event with:
* `event_start`: 2022-07-29
* `days`: 2
* `creation_time`: 20:00

<img src={require('@site/static/gif/repeating_event_create.gif').default} />

When I executed the command, my local time was _2022-07-27_ at _20:27_. Since the event `event_start` (2022-07-29) minus the _2_ `days` and a `creation_time` of _20:00_ is already in the past, the event was created immediately.

Since (by default) repeating events are created every 7 days, the next event should be created at _2022-08-03_ with an event date of _2022-08-05_. Use `/repeating_event list` to verify that.

<img src={require('@site/static/img/repeating_event_list.png').default} />

## Custom Interval
:::info
  Repeating events repeat every 7 days (by default). Other intervals require you to have [premium](https://raidorganizer.org/premium).
:::

<DiscordComponent>
  <DiscordMessage profile="raidorganizer" bot>
    <div slot="interactions">
      <DiscordInteraction profile="snawe" command>
        repeating_event
      </DiscordInteraction>
    </div>
    <DiscordEmbed
    embedTitle="Repeating Event Setup"
    borderColor="#EEE657"
    >
      Select in which interval you want the event to repeat.
    </DiscordEmbed>
      <DiscordButtons>
        <DiscordButton type="secondary">Every 7 Days (Default)</DiscordButton>
        <DiscordButton type="secondary">Every X Days</DiscordButton>
        <DiscordButton type="secondary">On Every X. Day In Month</DiscordButton>
        <DiscordButton type="secondary">On Every First/... Monday/... In Month</DiscordButton>
        <DiscordButton type="secondary">Every Monday/Tuesday/...</DiscordButton>
      </DiscordButtons>
  </DiscordMessage>
</DiscordComponent>

### Every X Days

This option lets you repeat the event in a custom days interval.  
For example every 10 days.

<DiscordComponent>
  <DiscordMessage profile="raidorganizer" bot>
    <div slot="interactions">
      <DiscordInteraction profile="snawe" command>
        repeating_event
      </DiscordInteraction>
    </div>
    <DiscordEmbed
    embedTitle="Repeating Event Setup"
    borderColor="#EEE657"
    >
      Select in which interval you want the event to repeat. <br />
      Repeat every X days:
    </DiscordEmbed>
      <DiscordButtons>
        <DiscordButton type="secondary" disabled>Every 7 Days (Default)</DiscordButton>
        <DiscordButton type="success" disabled>Every X Days</DiscordButton>
        <DiscordButton type="secondary" disabled>On Every X. Day In Month</DiscordButton>
        <DiscordButton type="secondary" disabled>On Every First/... Monday/... In Month</DiscordButton>
        <DiscordButton type="secondary" disabled>Every Monday/Tuesday/...</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="secondary">0</DiscordButton>
        <DiscordButton type="secondary">1</DiscordButton>
        <DiscordButton type="secondary">2</DiscordButton>
        <DiscordButton type="secondary">3</DiscordButton>
        <DiscordButton type="secondary">4</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="secondary">5</DiscordButton>
        <DiscordButton type="secondary">6</DiscordButton>
        <DiscordButton type="secondary">7</DiscordButton>
        <DiscordButton type="secondary">8</DiscordButton>
        <DiscordButton type="secondary">9</DiscordButton>
      </DiscordButtons>
  </DiscordMessage>
</DiscordComponent>

### On Every X. Day In Month

This option lets you repeat the event on every x. in a month.  
For example every 10. in a month.

:::note
 If you enter `31.`, months without 31 days are skipped.
:::  

<DiscordComponent>
  <DiscordMessage profile="raidorganizer" bot>
    <div slot="interactions">
      <DiscordInteraction profile="snawe" command>
        repeating_event
      </DiscordInteraction>
    </div>
    <DiscordEmbed
    embedTitle="Repeating Event Setup"
    borderColor="#EEE657"
    >
      Select in which interval you want the event to repeat. <br />
      Repeat on every X. in month:
    </DiscordEmbed>
      <DiscordButtons>
        <DiscordButton type="secondary" disabled>Every 7 Days (Default)</DiscordButton>
        <DiscordButton type="secondary" disabled>Every X Days</DiscordButton>
        <DiscordButton type="success" disabled>On Every X. Day In Month</DiscordButton>
        <DiscordButton type="secondary" disabled>On Every First/... Monday/... In Month</DiscordButton>
        <DiscordButton type="secondary" disabled>Every Monday/Tuesday/...</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="secondary" disabled>0</DiscordButton>
        <DiscordButton type="secondary">1</DiscordButton>
        <DiscordButton type="secondary">2</DiscordButton>
        <DiscordButton type="secondary">3</DiscordButton>
        <DiscordButton type="secondary">4</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="secondary">5</DiscordButton>
        <DiscordButton type="secondary">6</DiscordButton>
        <DiscordButton type="secondary">7</DiscordButton>
        <DiscordButton type="secondary">8</DiscordButton>
        <DiscordButton type="secondary">9</DiscordButton>
      </DiscordButtons>
  </DiscordMessage>
</DiscordComponent>

### On Every First/... Monday/... In Month

This option lets you repeat the event on every first/... Monday/... in a month.  
For example every second Friday in a month.

<DiscordComponent>
  <DiscordMessage profile="raidorganizer" bot>
    <div slot="interactions">
      <DiscordInteraction profile="snawe" command>
        repeating_event
      </DiscordInteraction>
    </div>
    <DiscordEmbed
    embedTitle="Repeating Event Setup"
    borderColor="#EEE657"
    >
      Select in which interval you want the event to repeat. <br />
    </DiscordEmbed>
      <DiscordButtons>
        <DiscordButton type="secondary" disabled>Every 7 Days (Default)</DiscordButton>
        <DiscordButton type="secondary" disabled>Every X Days</DiscordButton>
        <DiscordButton type="secondary" disabled>On Every X. Day In Month</DiscordButton>
        <DiscordButton type="success" disabled>On Every First/... Monday/... In Month</DiscordButton>
        <DiscordButton type="secondary" disabled>Every Monday/Tuesday/...</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="secondary" disabled>First</DiscordButton>
        <DiscordButton type="success" disabled>Second</DiscordButton>
        <DiscordButton type="secondary" disabled>Third</DiscordButton>
        <DiscordButton type="secondary" disabled>Fourth</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="secondary" disabled>Monday</DiscordButton>
        <DiscordButton type="secondary" disabled>Tuesday</DiscordButton>
        <DiscordButton type="secondary" disabled>Wednesday</DiscordButton>
        <DiscordButton type="secondary" disabled>Thursday</DiscordButton>
        <DiscordButton type="success" disabled>Friday</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="secondary" disabled>Saturday</DiscordButton>
        <DiscordButton type="secondary" disabled>Sunday</DiscordButton>
      </DiscordButtons>
  </DiscordMessage>
</DiscordComponent>

### Every Monday/Tuesday/...

This option lets you repeat the event on Monday/Tuesday/... in a week.  
For example every Monday and Friday.

<DiscordComponent>
  <DiscordMessage profile="raidorganizer" bot>
    <div slot="interactions">
      <DiscordInteraction profile="snawe" command>
        repeating_event
      </DiscordInteraction>
    </div>
    <DiscordEmbed
    embedTitle="Repeating Event Setup"
    borderColor="#EEE657"
    >
      Select in which interval you want the event to repeat. <br />
    </DiscordEmbed>
      <DiscordButtons>
        <DiscordButton type="secondary" disabled>Every 7 Days (Default)</DiscordButton>
        <DiscordButton type="secondary" disabled>Every X Days</DiscordButton>
        <DiscordButton type="secondary" disabled>On Every X. Day In Month</DiscordButton>
        <DiscordButton type="secondary" disabled>On Every First/... Monday/... In Month</DiscordButton>
        <DiscordButton type="success" disabled>Every Monday/Tuesday/...</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="success">Monday</DiscordButton>
        <DiscordButton type="secondary">Tuesday</DiscordButton>
        <DiscordButton type="secondary">Wednesday</DiscordButton>
        <DiscordButton type="secondary">Thursday</DiscordButton>
        <DiscordButton type="success">Friday</DiscordButton>
      </DiscordButtons>
      <DiscordButtons>
        <DiscordButton type="secondary">Saturday</DiscordButton>
        <DiscordButton type="secondary">Sunday</DiscordButton>
      </DiscordButtons>
  </DiscordMessage>
</DiscordComponent>
