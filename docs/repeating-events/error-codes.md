---
title: Error Codes
description: Description of error codes
---

The creation of an event, from a repeating event entry, can fail in different ways. Since I think that it is not a good idea to let the bot send "random" messages to the chat, the bot fails in silent and sets an error code to the repeating event entry. 

:::info 
 You can use the command `/repeating_event list` to get an overview of all repeating entries that currently exist on your server. 
:::

The following error codes exist:

| Error Code/Text                                       | Meaning                                                                                                                                                                                                                                       |
|-------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Error 2: No timezone set                              | The server has no `timezone` set. Use `timezone set` to fix it.                                                                                                                                                                               |
| Error 3: Invalid channel                              | The bot can no longer access the channel in which the event should be created. Either because of missing permission, or because the channel was deleted. Use `/permission` to check the permissions of the bot in that channel.               |
| Error 4: Invalid channel                              | Same as `Error 3: Invalid channel`.                                                                                                                                                                                                           |
| Error 5: Invalid signup template                      | The configured `signup template` no longer exists. You have either deleted it or performed some breaking changes on it.                                                                                                                       |
| Error 6: Could not create event message               | Event creation failed. Most likely the bot is missing some permissions in the event channel. Verify it with the command `/permission` in the given channel.                                                                                   |
| Error 7: Something went wrong during date calculation | Something went wrong when the bot calculated the date for the next event creation. If that happens, it is very likely a bug. Please report it on the [discord server](https://discord.com/channels/658007285205041153/658020529743069224).    |
| Error 8: Invalid repeating event entry                | The repeating event entry has some invalid config. This happens for example if your server had `premium` and used a custom interval. Now `premium` runs out and the entry gets deactivated.                                                   |
| Error 9: Not a valid event channel                    | Happens if you use `/channel remove` to remove the event/lfg in which this repeating entry should create an event.                                                                                                                            |
