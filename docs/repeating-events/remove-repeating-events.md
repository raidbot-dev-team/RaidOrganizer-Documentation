---
title: Remove Repeating Events
description: Remove a Repeating Event
---

To remove a repeating event use the `/repeating_event remove` command.

:::note
If you remove a repeating event entry, you are **not** actually deleting an event! Instead you are deleting the entry from which further events would have been created. Already created events will **not** be effected.
:::
