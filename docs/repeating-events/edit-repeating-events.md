---
title: Edit Repeating Events
description: Edit a Repeating Event
---

To edit a repeating event use the `/repeating_event edit` command.

:::note
If you edit a repeating event entry, you are **not** actually editing an event! Instead you are editing _how_ further events will be created. Already created events will **not** be effected.
:::
