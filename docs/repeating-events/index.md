import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

# Repeating Events

You want to create an event that repeats itself every X days? Learn about them here!

## Some Basics First

If we talk about "Repeating Events" we mean an entry in the database from which the actual event will be created.

Furthermore, this means that if you edit a repeating event entry, you are **not** actually editing an event. Instead you are editing _how_ further events will be created.

<DocCardList items={useCurrentSidebarCategory().items}/>