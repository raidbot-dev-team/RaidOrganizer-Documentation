---
title: Setup Server
---

## Configure Server Wide Settings

When invited, the bot will send a direct message (DM) to the owner of the server. If you are not the owner or you missed the DM, default values are used. You can check your server wide configuration with `/server config`.

:::note
 To change server settings you need to be either [**server owner**](/getting-started/terms#server-owner), have [**administrator privileges**](/getting-started/terms#administrator) or have the role that is set as [**moderator role**](/getting-started/terms#moderator-role)!
:::
:::info
`/moderator_role set` command, which changes the [**moderator role**](/getting-started/terms#moderator-role), can only be used by a user that either has [**administrator privileges**](/getting-started/terms#administrator) or is the [**server owner**](/getting-started/terms#server-owner)!
:::

If you missed the initial DM you can also use the following commands:
* `/channel setup` will setup the given channel. Read [Setup Channel](./setup-channel) for more information.
* `/date_format set` to set the **format** of how the bot accepts and shows the date.
* `/timezone set` to tell the bot in which timezone the start/end time of events are. (Only needed if you use start/end times for events)

## Other Server Settings

Now that you have set up your server for the bot, there are a few more serverwide settings!

Server configurations are, as the name suggest, serverwide. This means that a `date_format` applies to all channels and events in that server.

:::note
 You can use the `help` command and select the "Server" category...  
 Or use the `/server config` command to get a list of server-wide settings!
:::

