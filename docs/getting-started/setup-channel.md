---
title: Setup Channel
---

## Setup a Channel for Events

By design **Raid Organizer** uses so called "Event Channels" in which events can be created. Since a lot of the configuration is channel based, you first need to initialize a channel before you can run events in it.
:::note
 To get a list of event channels, use the `/channel list` command.  
 To get a list of the current channel configuration, use the `/channel config` command.  
 To get a list of all channel based commands, use the `/help` command and select the "Channel" category.
:::

To setup a channel use the `channel setup` command. The command takes 3 parameters:

* `Type`: Allowed channel types are `event` and `lfg`.
    * `event`: Event channels are the classic way of organizing events or raids. Users with the [organizer role](/getting-started/terms#organizer-role) are able to create events and manage events. Users with the [user role](/getting-started/terms) are able to sign up for those events.
    :::note
    An organizer also needs the user role if he/she wants to be able to sign up for events.
    :::
    * `lfg`: LFG channels are perfect if you want to allow your members to create their own dungeon runs/lfg's/events. In this mode every user with the [user role](/getting-started/terms#user-role) is able to create and manage their own events. The [organizer role](/getting-started/terms#organizer-role) is used as "moderator". Which means they can manage events (close, remove, ...) from all users.
* `Organizer`: Users that have the [organizer role](/getting-started/terms#organizer-role) are allowed to create, delete, edit, ... events. Basically they can manage the events inside an event channel.
* `User`: Users that have the [user role](/getting-started/terms#user-role) can join events over reactions. In channels of the type `lfg`, users with the [user role](/getting-started/terms#user-role) can also create and manage their own events.

**Example:**
<img src={require('@site/static/gif/setup_channel.gif').default} />

## Other Channel Configs

Now that you have set up a channel for the bot, there are a few more options to fully customize the bot!

Channel configurations are, as the name suggests, bound to a spezific channel. This means you can have different `default_template`'s, `export_channel`'s, etc. in each channel!

:::note
 You can use the `help` command and select the "Channel" category...  
 Or use the `/channel config` command to get a list of channelwide settings!
:::

