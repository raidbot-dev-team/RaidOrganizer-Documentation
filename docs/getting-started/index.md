---
description: Set up Raid Organizer and create your first event
keywords: [set up, Raid Organizer, getting started]
---

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

# Getting Started

In 3 steps to your first event:
* Setup your server (optional)
* Setup a channel in which the bot will create events
* Create your first event

<DocCardList items={useCurrentSidebarCategory().items}/>