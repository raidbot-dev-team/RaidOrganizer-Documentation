---
title: Create Your First Event
description: Create an Event
---

You are now ready to create your first event! To do that use the `/create event` command.

<img src={require('@site/static/gif/event_create_dark.gif').default} />

:::note
 The fields `title` and `event_start` are mandatory. All other fields are optional!
:::

Read more about events [here](/events/).