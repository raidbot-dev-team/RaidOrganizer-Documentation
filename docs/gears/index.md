# Gears

Gears are a currency for the `Raid Organizer`-Bot. With gears you can increase your server `/limits` forever and exchange them for [premium](https://raidorganizer.org/premium).

## Get Gears

You can get gears by:
* Voting for the bot on [top.gg](https://top.gg/bot/658698521570181140) and [discordbotlist.com](https://discordbotlist.com/bots/raid-organizer) (1 gear for each vote)

## Use Gears

The gears can be exchanged in our [official Discord server](http://discord.raidorganizer.org) in the `⛭gear-exchange` channel.

**User/Guild member** can move gears from their own wallet to the guild wallet by using the `/gears user contribute` command.

**Owner/Admins/Moderators** can use gears in the guild wallet to exchange them to increase the server limits (_channels_, _templates_, etc.) or to activate [_premium_](https://raidorganizer.org/premium) _features_! To do that, use the `/gears guild consume` command.
