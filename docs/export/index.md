import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

# Export Event/User statistics

`Raid Organizer` has multiple options to get event and user statistics. Additionally to using a [log channel](/logging/) you can also

<DocCardList items={useCurrentSidebarCategory().items}/>
