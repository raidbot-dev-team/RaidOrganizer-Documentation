---
title: Get User Sign ups
description: Get sign ups of users in a period
---

The command `/user signups get` returns all users that meet your passed requirements.

The command takes 3 arguments + 1 optional
* `limit` = "minimum" or "maximum
* `amount` = upper or lower limit. Can be a number or percent
* `days` = Amount of days to go back from current date to search for events.
* (optional) `export_csv` => Default false. If true, a csv file is added to the message.

:::info
  * Only users that have signed up to a `role` emoji are considered. `status` sign ups do **not** count.
  * Only existing events are checked. So it depends on the "event deletion time" how far to the past the bot can search for users. Manually deleted events are also not taken into account.
:::

## Examples 

You have 10 events within the last 14 days.

* `User A` is signed up to 5 events
* `User B` is signed up to 3 events
* `User C` is signed up to 8 events

### Example 1

If you use the command with:
* `limit` = "minimum"
* `amount` = 5
* `days` = 14

=> Bot returns `User A` and `User C` because both have or more than (or equal to) 5 sign ups.

### Example 2

If you use the command with:
* `limit` = "maximum"
* `amount` = 3
* `days` = 14

=> Bot returns `User B` because only `User B` has less than (or equal to) 3 sign ups.

### Example 3

If you use the command with:
* `limit` = "maximum"
* `amount` = 60%
* `days` = 14

=> Bot returns `User A` and `User B` because both have less than 60% attendance.