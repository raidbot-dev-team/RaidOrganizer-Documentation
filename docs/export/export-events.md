---
title: Export Events
description: Export events to a given channel
---

`Raid Organizer` can dump an event to a set export channel. This way you can either have snapshots of the event at a given time or archive events for the "eternity".

:::info
  `Raid Organizer` automatically deletes events from the event channel after the `/event_deletion_time` is over. This has 2 reasons:

  1) Keep the channel clean.  
  2) Remove no longer active events from the database for GDPR reasons.
:::

## Setup

There are a couple of commands to let you customize the export experience.

* [/export_event channel set](#set-an-export-channel)
* [/export_event add_file](#configure-file-attachment)
* [/export_event toggle](#enable-automatic-export)
* [/event export](#manual-export)

### Set an Export Channel

With the `/export_event channel set` command you can set the channel to which events are exported.

:::note
  `Raid Organizer` only lists channels to which access are granted.
:::
:::note
  This is an auto complete field. If a channel is not listed, start typing it's name.
:::

### Configure File Attachment

By default only the event message is duplicated. However, `Raid Organizer` also has the ability to add a `.csv` or `.json` file with additional information.

The `/export_event add_file` command lets you choose if a `.csv` or `.json` file shall be attached to the export message.

If a file is added, you can also choose what users are listed in it. By default only signed up users are exported. The export information contains:

* Discord User ID
* Discord User Name
* Sign Up Emoji *
* Sign Up Role Info *
* Late Status *
* Confirmed Status *

\* only for users that are signed up to the given event.

### Enable Automatic Export

With the `/export_event toggle` command you can enable automatic event export. Events get exported on [event closure](/channel_settings/event_closure).

### Manual Export

You can also export an event at any given time to create some sort of a "snapshot". This can be done via `/event export`.