import React, { createContext } from 'react';
import {
	DiscordDefaultOptions,
	DiscordOptionsContext,
	DiscordMessageOptions,
	DiscordMessages,
} from '@discord-message-components/react'
import '@discord-message-components/react/dist/style.css'
// import Twemoji from 'react-twemoji';

export const defaultOptions: DiscordMessageOptions = {
    ...DiscordDefaultOptions,
    profiles: {
        snawe: {
            author: 'Snawe',
            avatar: '/gif/snawe_icon.gif',
            roleColor: '#ffe800',
            ephemeral: true,
        },
        raidorganizer: {
            author: 'Raid Organizer',
            avatar: '/img/raid_organizer_icon.png',
            roleColor: '#de2800',
            bot: true,
            ephemeral: true,
        },
    },
};

interface DiscordComponentProps {
    options?: DiscordMessageOptions
}

const DiscordComponent: React.FC<DiscordComponentProps> = ({options = defaultOptions, children }) => {
    return (
        <DiscordOptionsContext.Provider value={options}>
            <DiscordMessages>
                {children}
            </DiscordMessages>
        </DiscordOptionsContext.Provider>
    );
};

export default DiscordComponent;