/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
  docs: [
    'home',
    {
      type: 'category',
      label: 'Getting Started',
      link: {type: 'doc', id: 'getting-started/index'},
      items: [
        'getting-started/setup-server',
        'getting-started/setup-channel',
        'getting-started/create-event',
        'getting-started/terms'
      ]
    },
    // Features
    {
      type: 'html',
      value: '<fieldset class="sidebar-divider"><legend>Features</legend></fieldset>',
    },
    'discord-events/index',
    {
      type: 'category',
      label: 'Events',
      link: {type: 'doc', id: 'events/index'},
      items: [
        'events/create-events',
        'events/edit-events',
        'events/remove-events',
      ]
    },
    {
      type: 'category',
      label: 'Event Notification',
      link: {type: 'doc', id: 'notification/index'},
      items: [
        'notification/notify',
        'notification/event-start-notification',
        'notification/event-creation-notification',
        'notification/event-cancel-notification',
      ]
    },
    {
      type: 'category',
      label: 'Event Templates',
      link: {type: 'doc', id: 'event-templates/index'},
      items: [
        'event-templates/setup-event-template',
        'event-templates/create-event-template',
        'event-templates/remove-event-template',
      ]
    },
    {
      type: 'category',
      label: 'Export User/Event Statistics',
      link: {type: 'doc', id: 'export/index'},
      items: [
        'export/user-signups-get',
        'export/export-events',
      ]
    },
    'logging/index',
    {
      type: 'category',
      label: 'Repeating Events',
      link: {type: 'doc', id: 'repeating-events/index'},
      items: [
        'repeating-events/create-repeating-events',
        'repeating-events/edit-repeating-events',
        'repeating-events/remove-repeating-events',
        'repeating-events/error-codes',
      ]
    },
    {
      type: 'category',
      label: 'Signup Templates',
      link: {type: 'doc', id: 'signup-templates/index'},
      items: [
        'signup-templates/create-template',
        'signup-templates/import-template',
        'signup-templates/edit-template',
        'signup-templates/remove-template',
      ]
    },
    {
      type: 'category',
      label: 'Upcoming Event Messages',
      link: {type: 'doc', id: 'upcoming/index'},
      items: [
        'upcoming/upcoming-events',
        'upcoming/upcoming-message',
      ]
    },
    'voice-channel/index',
    // Miscellaneous
    {
      type: 'html',
      value: '<fieldset class="sidebar-divider"><legend>Settings</legend></fieldset>',
    },
    {
      type: 'category',
      label: 'Channel Settings',
      link: {type: 'doc', id: 'channel_settings/index'},
      items: [
        'channel_settings/display_name',
        'channel_settings/event_closure',
        'channel_settings/event_create_overlap',
        'channel_settings/show_user_choices'
      ]
    },
    // Miscellaneous
    {
      type: 'html',
      value: '<fieldset class="sidebar-divider"><legend>Miscellaneous</legend></fieldset>',
    },
    'gears/index',
    'permissions/index',
    {
      type: 'category',
      label: 'Premium',
      link: {type: 'doc', id: 'premium/index'},
      items: [
        'premium/gears',
        {
          type: 'category',
          label: 'Via Subscription (Patreon)',
          link: {
            type: 'doc',
            id: 'premium/patreon/index',
          },
          items: [
            'premium/patreon/assign_key',
            'premium/patreon/up_downgrade',
          ],
        },
        'premium/paypal',
        'premium/trial',
      ],
    },
    // // Divider
    // {
    //   type: 'html',
    //   value: '<span class="sidebar-divider" />',
    // },
    // // Title
    // {
    //   type: 'html',
    //   defaultStyle: true,
    //   className: 'add-banner',
    //   value: `
    //     <span>Advertisement</span>
    //   `,
    // },
    // // Text
    //   {
    //     type: 'html',
    //     defaultStyle: true,
    //     value: `
    //       <p style="font-size:75%";>Use the links below to support <code>Raid Organizer</code>.</p>
    //     `,
    //   },
    // // Adds
    // {
    //   type: 'html',
    //   defaultStyle: true,
    //   className: 'add-banner',
    //   value: `
    //   <!--Basic structure of slider-->
    //   <div class="add_container">
    //   <!--Area of the images-->
    //      <div class="img_wrapper box">
    //         <div class="single-box"><a href="https://hetzner.cloud/?ref=eJn45HQkudQl" target="_blank"><img src="https://cloud.raidorganizer.org/s/pwQctr6tRwZtTHp/preview"></a></div>
    //         <div class="single-box"><a href="https://go.nordvpn.net/aff_c?offer_id=15&aff_id=62207&url_id=902" target="_blank"><img src="https://cloud.raidorganizer.org/s/fKdLoazwDNWB8Z8/preview"></a></div>
    //         <div class="single-box"><a href="https://www.amazon.com?&linkCode=ll2&tag=raidorganizer-20&linkId=a208f7e933f158e1c09376fc233af0e0&language=en_US&ref_=as_li_ss_tl" target="_blank"><img src="https://cloud.raidorganizer.org/s/dPsrCczQPkbgtL4/preview"></a></div>
    //      </div>
    //   </div>
    //   `,
    // },
  ]
};

module.exports = sidebars;
